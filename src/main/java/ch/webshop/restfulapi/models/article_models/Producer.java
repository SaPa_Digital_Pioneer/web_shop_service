package ch.webshop.restfulapi.models.article_models;

/**
 * Class represent object producer in  article document
 */
public class Producer {
    private String name;


    //Default constructor
    public Producer(){}

    //Constructor with parameter
    public Producer(String name) {
        this.name = name;
    }

    //Getter and Setter methods

    /**
     * @return name of producer
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
