package ch.webshop.restfulapi.models.article_models;
import ch.webshop.restfulapi.models.user_models.User;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

/**
 * Class represents data structure of the article document in web shop collection
 */
public class Article {

    @Id
    private String _id;
    private String user_email;
    private String article_name;
    private String article_price;
    private String article_amount;
    private String article_description;
    private Category category;
    private String[] images;
    private Producer producer;
    private ArrayList<Object> article_rating;


    //Default constructor
    public Article(){}

    //Constructor with parameter
    public Article(String _id, String user_email,String article_name, String article_price, String article_amount, String article_description, Category category, String[] images, Producer producer, ArrayList<Object> article_rating) {
        this._id = _id;
        this.user_email = user_email;
        this.article_name = article_name;
        this.article_price = article_price;
        this.article_amount = article_amount;
        this.article_description = article_description;
        this.category = category;
        this.images = images;
        this.producer = producer;
        this.article_rating = article_rating;
    }

    //Getter and Setter Methods
    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    /**
     * @return article object id, each article has his own id
     */
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    /**
     * @return name of an single article
     */
    public String getArticle_name() {
        return article_name;
    }

    public void setArticle_name(String article_name) {
        this.article_name = article_name;
    }

    /**
     * @return price of single article
     */
    public String getArticle_price() {
        return article_price;
    }

    public void setArticle_price(String article_price) {
        this.article_price = article_price;
    }

    /**
     * @return how much article are available
     */
    public String getArticle_amount() {
        return article_amount;
    }

    public void setArticle_amount(String article_amount) {
        this.article_amount = article_amount;
    }

    /**
     * @return all about articles properties
     */
    public String getArticle_description() {
        return article_description;
    }

    public void setArticle_description(String article_description) {
        this.article_description = article_description;
    }

    /**
     * @return the category of an single article
     */
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * @return all article images
     */
    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    /**
     * @return who made this article
     */
    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    /**
     * @return users rating about the article
     */
    public ArrayList<Object>getArticle_rating() {
        return article_rating;
    }

    public void setArticle_rating(ArrayList<Object> article_rating) {
        this.article_rating = article_rating;
    }
}
