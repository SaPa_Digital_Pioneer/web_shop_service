package ch.webshop.restfulapi.models.article_models;
/**
 * Part of article document.
 * Data structure of object category
 */
public class Category {
    private String name;

    //Default constructor
    public Category(){}

    //Constructor with parameter

    public Category(String name) {
        this.name = name;
    }
    // Getter and Setter methods

    /**
     * @return name of category
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
