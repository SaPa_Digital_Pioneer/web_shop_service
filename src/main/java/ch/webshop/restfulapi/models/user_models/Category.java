package ch.webshop.restfulapi.models.user_models;

import org.springframework.data.annotation.Id;

public class Category {
    @Id
    private String _id;
    private String category_name;

    //Constructor
    public Category(String _id, String category_name) {
        this._id = _id;
        this.category_name = category_name;
    }

    //Getter and Setter
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }
}


