package ch.webshop.restfulapi.models.user_models;

public class Location {

    public static double distance(Geo geoLoc1, Geo geoLoc2, char unit) {
        double theta = geoLoc1.getLongitude() - geoLoc2.getLongitude();
        double dist = Math.sin(deg2rad(geoLoc1.getLatitude())) * Math.sin(deg2rad(geoLoc2.getLatitude())) + Math.cos(deg2rad(geoLoc1.getLatitude())) * Math.cos(deg2rad(geoLoc2.getLatitude())) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == 'K') {
            dist = dist * 1.609344;
        } else if (unit == 'N') {
            dist = dist * 0.8684;
        }
        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

}
