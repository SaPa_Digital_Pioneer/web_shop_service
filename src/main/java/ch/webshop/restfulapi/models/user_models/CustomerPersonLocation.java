package ch.webshop.restfulapi.models.user_models;

/**
 * CustomerPerson object holds CustomerPersonLocation object
 * Class represents data structure of object in user document
 */
public class CustomerPersonLocation {

    private String city;
    private int postcode;
    private int housenumber;
    private String phone;

    //Default constructor
    public CustomerPersonLocation(){}

    //Constructor with parameter
    public CustomerPersonLocation(String city, int postcode, int housenumber, String phone) {
        this.city = city;
        this.postcode = postcode;
        this.housenumber = housenumber;
        this.phone = phone;
    }

    //Getter and Setter Methods

    /**
     * @return city where customer life
     */
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return postcode of customer address
     */
    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    /**
     * @return house number of customers house
     */
    public int getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(int housenumber) {
        this.housenumber = housenumber;
    }

    /**
     * @return customers phone number
     */
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
