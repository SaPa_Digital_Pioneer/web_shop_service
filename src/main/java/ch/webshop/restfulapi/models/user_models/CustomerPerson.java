package ch.webshop.restfulapi.models.user_models;

/**
 * Object is part of user document
 * Class represents data structure in the user document
 */
public class CustomerPerson {

    private String customer_person_name;
    //Holds the rating for each single customer
    private String [] customer_person_rating;
    private CustomerPersonLocation customer_person_location;

    //Default constructor
    public CustomerPerson(){}

    //Constructor with parameter


    public CustomerPerson(String customer_person_name, String[] customer_person_rating, CustomerPersonLocation customer_person_location) {
        this.customer_person_name = customer_person_name;
        this.customer_person_rating = customer_person_rating;
        this.customer_person_location = customer_person_location;
    }

    // Getter and Setter Methods

    /**
     * @return the customer name
     */
    public String getCustomer_person_name() {
        return customer_person_name;
    }

    public void setCustomer_person_name(String customer_person_name) {
        this.customer_person_name = customer_person_name;
    }

    /**
     * @return rating of an single customer added by wb shop users
     */
    public String[] getCustomer_person_rating() {
        return customer_person_rating;
    }

    public void setCustomer_person_rating(String[] customer_person_rating) {
        this.customer_person_rating = customer_person_rating;
    }

    /**
     * @return object of customers location
     */
    public CustomerPersonLocation getCustomer_person_location() {
        return customer_person_location;
    }

    public void setCustomer_person_location(CustomerPersonLocation customer_person_location) {
        this.customer_person_location = customer_person_location;
    }
}
