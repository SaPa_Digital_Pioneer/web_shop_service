package ch.webshop.restfulapi.models.user_models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

/**
 * User document holds an object of all orders the user has been made
 */
public class UserOrder {
    @Id
    private String arcticel_id;
    private String payment;

    // Default Constructor w
    public UserOrder() {
    }

    // Constructor with all parameter
    public UserOrder(String arcticel_id, String payment) {
        this.arcticel_id = arcticel_id;
        this.payment = payment;
    }

    // Getter and Setter Methods
    public String getArcticel_id() {
        return arcticel_id;
    }

    public void setArcticel_id(String arcticel_id) {
        this.arcticel_id = arcticel_id;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }
}
