package ch.webshop.restfulapi.models.user_models;

/**
 * Object in customer person object.
 * Object represents data structr in user documents on MongoDB
 */
public class SalesPersonLocation {

    private String city;
    private int postcode;
    private String address;
    private String phone;

    // Default constructor
    public SalesPersonLocation(){}

    // Constructor with all parameter
    public SalesPersonLocation(String city, int postcode, String address, String phone) {
        this.city = city;
        this.postcode = postcode;
        this.address = address;
        this.phone = phone;
    }

    /**
     * @return in which city the customer is located
     */
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return postcode of customers address
     */
    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    /**
     * @return phone number customer
     */
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return this.address + ", " + this.city + ", " + this.postcode;
    }
}
