package ch.webshop.restfulapi.models.user_models;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

public class User {

    // Id-Annotation tells Spring use this filed as primary identifier
    @Id
    private String _id;
    private String user_first_name;
    private String user_last_name;
    private String user_email;
    private String user_pwd;
    private String user_rule_id;
    private UserOrder user_order;
    private UserLocation user_location;
    private SalesPerson sales_person;
    private CustomerPerson customer_person;

    // Constructor
   public  User(){}

    // Constructor with parameter
    public User(String _id, String user_first_name, String user_last_name, String user_email, String user_pwd, String user_rule_id, UserOrder user_order, UserLocation user_location, SalesPerson sales_person, CustomerPerson customer_person) {
        this._id = _id;
        this.user_first_name = user_first_name;
        this.user_last_name = user_last_name;
        this.user_email = user_email;
        this.user_pwd = user_pwd;
        this.user_rule_id = user_rule_id;
        this.user_order = user_order;
        this.user_location = user_location;
        this.sales_person = sales_person;
        this.customer_person = customer_person;
    }

    public String getUser_first_name() {
        return user_first_name;
    }

    public String getUser_last_name() {
        return user_last_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public String getUser_pwd() {
        return user_pwd;
    }

    public void setUser_pwd(String user_pwd) {
        this.user_pwd = user_pwd;
    }

    public String getUser_rule_id() {
        return user_rule_id;
    }

    public void setUser_rule_id(String user_rule_id) {
        this.user_rule_id = user_rule_id;
    }

    public UserOrder getUser_order() {
        return user_order;
    }

    public void setUser_first_name(String user_first_name) {
        this.user_first_name = user_first_name;
    }

    public void setUser_last_name(String user_last_name) {
        this.user_last_name = user_last_name;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public void setUser_order(UserOrder user_order) {
        this.user_order = user_order;
    }

    public UserLocation getUser_location() {
        return user_location;
    }

    public void setUser_location(UserLocation user_location) {
        this.user_location = user_location;
    }

    public SalesPerson getSales_person() {
        return sales_person;
    }

    public void setSales_person(SalesPerson sales_person) {
        this.sales_person = sales_person;
    }

    public CustomerPerson getCustomer_person() {
        return customer_person;
    }

    public void setCustomer_person(CustomerPerson customer_person) {
        this.customer_person = customer_person;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_id() {
        return _id;
    }
}
