package ch.webshop.restfulapi.models.user_models;

/**
 * The SalePerson is inside the user document.
 * This MongoDB Schema Pattern realize an one to many relationship
 * This class is the representation of the SalesPerson object in the user document
 */


public class SalesPerson {

    private String sales_person_name;
    private String sales_description;
    private String sales_logo;
    private String sales_category;
    private String [] sales_person_rating;
    private SalesPersonLocation sales_person_location;

    //Default constructor
    public SalesPerson(){}

    // Constructor with all parameter
    public SalesPerson(String sales_person_name, String sales_description, String sales_logo, String sales_category, String[] sales_person_rating, SalesPersonLocation sales_person_location) {
        this.sales_person_name = sales_person_name;
        this.sales_description = sales_description;
        this.sales_logo = sales_logo;
        this.sales_category = sales_category;
        this.sales_person_rating = sales_person_rating;
        this.sales_person_location = sales_person_location;
    }

    /**
     * @return
     */
    public String getSales_person_name() {
        return sales_person_name;
    }

    public void setSales_person_name(String sales_person_name) {
        this.sales_person_name = sales_person_name;
    }

    /**
     * @return rating sales person added by the web shop user
     */
    public String[] getSales_person_rating() {
        return sales_person_rating;
    }

    public void setSales_person_rating(String[] sales_person_rating) {
        this.sales_person_rating = sales_person_rating;
    }

    /**
     * @return location of sales person
     */
    public SalesPersonLocation getSales_person_location() {
        return sales_person_location;
    }

    public void setSales_person_location(SalesPersonLocation sales_person_location) {
        this.sales_person_location = sales_person_location;
    }

    public String getSales_description() {
        return sales_description;
    }

    public void setSales_description(String sales_description) {
        this.sales_description = sales_description;
    }

    public String getSales_logo() {
        return sales_logo;
    }

    public void setSales_logo(String sales_logo) {
        this.sales_logo = sales_logo;
    }

    public String getSales_category() {
        return sales_category;
    }

    public void setSales_category(String sales_category) {
        this.sales_category = sales_category;
    }
}
