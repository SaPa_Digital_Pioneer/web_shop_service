package ch.webshop.restfulapi.models.user_models;

import org.springframework.data.annotation.Id;

/**
 * Represents the object  user location in user document
 */
public class UserLocation {

    private String city;
    private String postcode;
    private String housenumber;
    private String country;

    // Constructor
    public UserLocation(){}

    // Constructor with parameters
    public UserLocation(String city, String postcode, String housenumber, String country) {
        this.city = city;
        this.postcode = postcode;
        this.housenumber = housenumber;
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
