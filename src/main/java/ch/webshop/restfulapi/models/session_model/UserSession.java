package ch.webshop.restfulapi.models.session_model;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class UserSession {

    private String sessionId;
    private String user_email;

    //Constructor
    public UserSession() {
    }

    public UserSession(String sessionId, String user_email) {
        this.sessionId = sessionId;
        this.user_email = user_email;
    }

    //Getter and Setter
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }
}
