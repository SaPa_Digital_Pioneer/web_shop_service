package ch.webshop.restfulapi.repositories;

import ch.webshop.restfulapi.models.article_models.Article;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends MongoRepository<Article,String>, ArticleCustomService{
    Article findBy_id(String _id);
    List<Article> findArticleByUser_email(String user_email);
}
