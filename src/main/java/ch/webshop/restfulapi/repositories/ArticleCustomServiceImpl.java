package ch.webshop.restfulapi.repositories;
import ch.webshop.restfulapi.models.article_models.Article;
import ch.webshop.restfulapi.models.session_model.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

public class ArticleCustomServiceImpl implements ArticleCustomService{

    @Autowired
    MongoTemplate mongoTemplate;

    //Logger for logging methods
    private static final Logger LOGGER = LoggerFactory.getLogger(ArticleCustomServiceImpl.class);

    @Override
    public List<Article> findArticleByUser_email(String user_email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("user_email").is(user_email));
        List<Article> articles = mongoTemplate.find(query, Article.class);
        return articles;
    }
}
