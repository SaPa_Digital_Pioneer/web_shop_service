package ch.webshop.restfulapi.repositories;

import ch.webshop.restfulapi.models.article_models.Article;
import ch.webshop.restfulapi.models.user_models.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User,String>,UserCustomService{

    User findBy_id(String _id);

    public boolean getLoginUser(String user_email,String user_pwd);

    public boolean hasEmail(String user_email);

}
