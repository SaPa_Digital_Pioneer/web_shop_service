package ch.webshop.restfulapi.repositories;

import ch.webshop.restfulapi.models.article_models.Article;
import java.util.List;

public interface ArticleCustomService{
    List<Article> findArticleByUser_email(String user_email);
}
