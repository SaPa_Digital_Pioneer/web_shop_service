package ch.webshop.restfulapi.repositories;
import ch.webshop.restfulapi.models.user_models.Category;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CategoryRepository extends MongoRepository<Category,String> {
}
