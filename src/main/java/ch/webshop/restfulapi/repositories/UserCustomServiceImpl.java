package ch.webshop.restfulapi.repositories;
import ch.webshop.restfulapi.models.user_models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserCustomServiceImpl implements UserCustomService {


     //Logger for logging methods
     private static final Logger LOGGER = LoggerFactory.getLogger(UserCustomServiceImpl.class);

     @Autowired
     BCryptPasswordEncoder bCryptPasswordEncoder;
     @Autowired
     MongoTemplate mongoTemplate;

    /**
     *
     * @param user_email user email given by request mapping
     * @param user_pwd user password given by request mapping
     * @return true if user by given email and password is available in mongodb
     */
    @Override
    public  boolean getLoginUser(String user_email, String user_pwd) {

         //Decorate variable from method parameters
         String userPwdEncrypted;
         String userPlainText = user_pwd;

         //Customize query for found user by email
         Query query = new Query();
         query.addCriteria(Criteria.where("user_email").is(user_email));
         User userExist =  mongoTemplate.findOne(query,User.class);


         //Check user object if empty cause of NullPointException
         if(userExist.getUser_pwd()== null){
             return false;
         }
         //In case user object is not empty, get user password
         userPwdEncrypted = userExist.getUser_pwd();

         /**
          *Use BCrypt method to check plain text pwd with hash pwd
          *BCrypt encrypted pwd will never decrypted, thx matcher-method.
          *@return true if plain text pwd matches with encrypted pwd
          */
         if(bCryptPasswordEncoder.matches(userPlainText,userPwdEncrypted)){

            return true;
        }

        return false;
    }

    @Override
    public boolean hasEmail(String user_email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("user_email").is(user_email));
        User userExist =  mongoTemplate.findOne(query,User.class);

        return userExist != null;
    }
}
