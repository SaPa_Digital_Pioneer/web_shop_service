package ch.webshop.restfulapi.repositories;

import ch.webshop.restfulapi.models.session_model.UserSession;
import ch.webshop.restfulapi.models.user_models.User;
import com.mongodb.client.result.DeleteResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class UserSessionCustomServiceImpl implements UserSessionCustomService {

    @Autowired
    MongoTemplate mongoTemplate;

    //Logger for logging methods
    private static final Logger LOGGER = LoggerFactory.getLogger(UserSessionCustomServiceImpl.class);

    @Override
    public boolean addNewUserSession(String sessionId, String user_email) {

        if(sessionId !=null && user_email!=null) {
            UserSession userSession = new UserSession();
            userSession.setSessionId(sessionId);
            userSession.setUser_email(user_email);
            mongoTemplate.insert(userSession);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteUserSession(String sessionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("sessionId").is(sessionId));
        DeleteResult deletedUserSession = mongoTemplate.remove(query,UserSession.class);

        if (deletedUserSession.getDeletedCount()>=0){
            LOGGER.info(deletedUserSession.toString());
            return true;
        }

        return false;
    }

    @Override
    public String getUserSessionByUser_email(String user_email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("user_email").is(user_email));
        List<UserSession> sessions = mongoTemplate.find(query, UserSession.class);
        return sessions.size() > 0 ? sessions.get(0).getSessionId() : "";
    }

    @Override
    public boolean getUserSessionBySessionId(String sessionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("sessionId").is(sessionId));
        List<UserSession> sessions = mongoTemplate.find(query, UserSession.class);

        return sessions.size() > 0;
    }
}
