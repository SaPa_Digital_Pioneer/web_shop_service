package ch.webshop.restfulapi.repositories;

import ch.webshop.restfulapi.models.user_models.User;

import java.util.List;

public interface UserCustomService {

    public boolean getLoginUser(String user_email, String user_pwd);

    public boolean hasEmail(String user_email);
}
