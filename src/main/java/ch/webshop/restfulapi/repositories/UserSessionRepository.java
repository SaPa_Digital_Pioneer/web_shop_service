package ch.webshop.restfulapi.repositories;

import ch.webshop.restfulapi.models.session_model.UserSession;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserSessionRepository extends MongoRepository<UserSession,String>,UserSessionCustomService {

    //Add new Session Id into DB after login
    public boolean addNewUserSession(String sessionId, String user_email);

    //Delete Session Id in DB after logout
    public boolean deleteUserSession(String sessionId);

    //Get Session Id in DB after logout
    public String getUserSessionByUser_email(String user_email);

    public boolean getUserSessionBySessionId(String sessionId);
}
