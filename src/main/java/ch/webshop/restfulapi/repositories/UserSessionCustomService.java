package ch.webshop.restfulapi.repositories;

public interface UserSessionCustomService {

    public boolean addNewUserSession(String sessionId, String user_email);

    public boolean deleteUserSession(String sessionId);

    //Get Session Id in DB after logout
    public String getUserSessionByUser_email(String user_email);

    public boolean getUserSessionBySessionId(String sessionId);
}
