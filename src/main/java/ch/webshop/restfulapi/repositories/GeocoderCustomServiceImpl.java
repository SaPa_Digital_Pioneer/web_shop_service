package ch.webshop.restfulapi.repositories;

import ch.webshop.restfulapi.models.user_models.Geo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;



@Service
public class GeocoderCustomServiceImpl {

    private static final String GEOCODING_RESOURCE = "https://geocode.search.hereapi.com/v1/geocode";
    private static final String API_KEY = "LSbxHW-SH4cxljjoxi0H5SjkktdQTxFE4jGl9c2eUPs";
    private static final Logger LOGGER = LoggerFactory.getLogger(UserCustomServiceImpl.class);


    public Geo getGeo(String query) throws IOException, InterruptedException {

        HttpClient httpClient = HttpClient.newHttpClient();

        String encodedQuery = URLEncoder.encode(query,"UTF-8");
        String requestUri = GEOCODING_RESOURCE + "?apiKey=" + API_KEY + "&q=" + encodedQuery;

        HttpRequest geocodingRequest = HttpRequest.newBuilder().GET().uri(URI.create(requestUri))
                .timeout(Duration.ofMillis(2000)).build();

        HttpResponse geocodingResponse = httpClient.send(geocodingRequest,
                HttpResponse.BodyHandlers.ofString());
        LOGGER.info("Geo Method was called");
        LOGGER.info(geocodingRequest.toString());

        ObjectMapper mapper = new ObjectMapper();

        JsonNode responseJsonNode = mapper.readTree(geocodingResponse.body().toString());
        JsonNode items = responseJsonNode.get("items");
        JsonNode position = items.get(0).get("position");

        Double lat =position.get("lat").asDouble();

        Double lng = position.get("lng").asDouble();

        return new Geo(lat,lng);
    }





}
