package ch.webshop.restfulapi;

import ch.webshop.restfulapi.repositories.UserCustomServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
public class WebshopApiApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserCustomServiceImpl.class);

	public static void main(String[] args) {


		SpringApplication.run(WebshopApiApplication.class, args);
		LOGGER.info("WEB API IS RUNNING");
	}

}
