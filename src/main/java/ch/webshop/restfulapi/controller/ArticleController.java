package ch.webshop.restfulapi.controller;

import ch.webshop.restfulapi.models.article_models.Article;
import ch.webshop.restfulapi.models.user_models.Category;
import ch.webshop.restfulapi.models.user_models.User;
import ch.webshop.restfulapi.repositories.ArticleRepository;
import ch.webshop.restfulapi.repositories.CategoryRepository;
import ch.webshop.restfulapi.repositories.UserCustomServiceImpl;
import ch.webshop.restfulapi.repositories.UserSessionRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/article")
public class ArticleController {

    //Spring generates automatic an article repository object
    @Autowired
    private ArticleRepository articleRepository;
    //User session repository object
    @Autowired
    private UserSessionRepository userSessionRepository;

    //User session repository object
    @Autowired
    private CategoryRepository categoryRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserCustomServiceImpl.class);


    // Get Article by given id
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Article getArticleById(@PathVariable("id") String id) {
        return articleRepository.findBy_id(id);
    }

    //Url extension to get all article from collection
    @GetMapping(value = "/all/article")
    public List<Article> getAllArticle(HttpServletRequest request) {

        return articleRepository.findAll();

    }

    /**
     *  Search article document by given name
     * @return article by given name.
      */
    @RequestMapping(path ="/by/name", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE},method = RequestMethod.POST)
    public List <Article> searchArticleByName(@Validated @RequestBody Article art,@RequestParam ("sessionId")String sessionId,HttpServletRequest request) {

        HttpSession currentSession = request.getSession();
        // SessionId active as long as within 120s
        if(currentSession.getId().equals(sessionId) ){
            Example<Article> a = Example.of(art);
            return articleRepository.findAll(a);
        }
        //If session id expired delete session from db and make session invalidate
        else if(!currentSession.getId().equals(sessionId)){
            userSessionRepository.deleteUserSession(sessionId);
            currentSession.invalidate();

        }

       return null;
    }

    /**
     *  Search article document by given name
     * @return article by given name.
     */
    @RequestMapping(path ="/getArticleByEmail", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE},method = RequestMethod.POST)
    public List<Article> searchArticleByEmail(@Validated @RequestBody String user_email, HttpServletRequest request) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode responseJsonNode = mapper.readTree(user_email);
        String email = responseJsonNode.get("user_email").asText();

        List<Article> articles = articleRepository.findAll();
        List<Article> foundArticles = new ArrayList<>();
        for(Article a : articles){
            if(a.getUser_email().equals(email)){
                foundArticles.add(a);
            }
        }

        return foundArticles;
    }

    /**
     * @param art article object injected by http Post method
     * @return article which was added into mongodb
     * @throws Exception
     */
    @RequestMapping(path ="/add", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE},method = RequestMethod.POST)
    public ResponseEntity addNewArticle(@Validated @RequestBody Article art,@RequestParam ("sessionId")String sessionId,HttpServletRequest request) throws Exception{
        HashMap<String, Object> articleResponse = new HashMap<>();

        //Get current session
        HttpSession currentSession = request.getSession();
        if(currentSession.getId().equals(sessionId) ){
            articleRepository.insert(art);
            articleResponse.put("article", art);
            return  new ResponseEntity<>(articleResponse, HttpStatus.OK);
        }
        //If session id expired delete session from db and make session invalidate
        else if(!currentSession.getId().equals(sessionId)){
            userSessionRepository.deleteUserSession(sessionId);
            currentSession.invalidate();

        }
        articleResponse.put("message", "Something went wrong");
        return  new ResponseEntity<>(articleResponse, HttpStatus.valueOf(409));
    }

    /**
     * Using CrudRepository save-method to update article
     * @param art article needs to update
     * @return updated article
     * @throws Exception
     */
    @PutMapping(path ="/update", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity updateArticle(@Validated @RequestBody Article art,@RequestParam ("sessionId")String sessionId,HttpServletRequest request) throws Exception{

        HashMap<String, Object> articleResponse = new HashMap<>();
        //Get current session
        HttpSession currentSession = request.getSession();
        if(currentSession.getId().equals(sessionId) ){
            articleRepository.save(art);
            articleResponse.put("article", art);
            return  new ResponseEntity<>(articleResponse, HttpStatus.OK);
        }
        //If session id expired delete session from db and make session invalidate
        else if(!currentSession.getId().equals(sessionId)){
            userSessionRepository.deleteUserSession(sessionId);
            currentSession.invalidate();
        }
        return null;
    }

    /**
     * @param id article id who needs to deleted
     * @return response to client article was successfully deleted
     * @throws Exception
     */
    @DeleteMapping("/delete")
    public ResponseEntity deleteArticle(@Validated @RequestParam("id") String id,@RequestParam ("sessionId")String sessionId,HttpServletRequest request)throws Exception{

        HashMap<String, Object> articleResponse = new HashMap<>();
        //Get current session
        HttpSession currentSession = request.getSession();
        if(currentSession.getId().equals(sessionId) ){
            articleRepository.deleteById(id);
            articleResponse.put("message", "Article was successfully deleted");
            return  new ResponseEntity<>(articleResponse, HttpStatus.OK);
        }
        //If session id expired delete session from db and make session invalidate
        else if(!currentSession.getId().equals(sessionId)){
            userSessionRepository.deleteUserSession(sessionId);
            currentSession.invalidate();
        }

        return null;
    }

    //Get all category
    @GetMapping(value = "/all/category")
    public List<Category> getAllCategory( HttpServletRequest request) {
        //Trigger SessionListener
        HttpSession currentSession = request.getSession();
        LOGGER.info("GET ALL CATEGORY METHOD WAS CALLED");
        return categoryRepository.findAll();
    }

    //Add new category into db
    @RequestMapping(path ="/add/category", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE},method = RequestMethod.POST)
    public ResponseEntity addNewCategory(@Validated @RequestBody Category category, HttpServletRequest request) throws Exception{
        HashMap<String, Object> articleResponse = new HashMap<>();
        categoryRepository.insert(category);
        articleResponse.put("Category", category);
        return  new ResponseEntity<>(articleResponse, HttpStatus.OK);
    }
}
