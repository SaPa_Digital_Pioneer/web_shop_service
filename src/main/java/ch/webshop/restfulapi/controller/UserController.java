package ch.webshop.restfulapi.controller;
import ch.webshop.restfulapi.models.user_models.Geo;
import ch.webshop.restfulapi.models.user_models.Location;
import ch.webshop.restfulapi.models.user_models.Login;
import ch.webshop.restfulapi.models.user_models.User;
import ch.webshop.restfulapi.repositories.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.json.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This class is the interface between web client and the API
 * The controller class makes the user available for requests
 * Using Spring annotation mapping to map get Request from the web client
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserCustomServiceImpl.class);

    //Spring generates automatic an repository object
    @Autowired
    private UserRepository usersRepository;
    //Password encoder object
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    //User session repository object
    @Autowired
    private UserSessionRepository userSessionRepository;

    @Autowired
    GeocoderCustomServiceImpl geoRepository;

    // Get User by given id
    @RequestMapping(path ="/id", method = RequestMethod.GET)
    public User getUserById(@RequestParam("id") String id,HttpServletRequest request){
        return usersRepository.findBy_id(id);
    }

    //Url extension to get all user from collection
    @GetMapping(path = "/all/user")
    public List<User> getAllUser(HttpServletRequest request, @RequestParam("sessionId")String sessionId) {

        //Trigger SessionListener
        HttpSession currentSession = request.getSession();
        LOGGER.info("GET ALL USER WAS CALLED");

        // SessionId active as long as within 120s
        if(currentSession.getId().equals(sessionId) ){
            return usersRepository.findAll();
        }

        //If session id expired delete session from db and make session invalidate
        else if(!currentSession.getId().equals(sessionId)){
            userSessionRepository.deleteUserSession(sessionId);
            currentSession.invalidate();
            return null;

        }
           return null;
    }

    /**
     * Search user document by given name
     * @return user by given name.
     */
    @RequestMapping(path ="/by/name", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE},method = RequestMethod.POST)
    public List <User> searchUserByNmae(@Validated @RequestBody User usr, HttpServletRequest request) {
        Example<User> a = Example.of(usr);
        return usersRepository.findAll(a);
    }

    /**
     * @param usr user object injected by http Post method
     * @return user which was added into mongodb
     * @throws Exception
     */
    @RequestMapping(path ="/add", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE},method = RequestMethod.POST)
    public ResponseEntity addNewUser(@Validated @RequestBody User usr,HttpServletRequest request) throws Exception{

        HashMap<String, Object> userResponse = new HashMap<>();
        if((usr.getUser_email() != null && usr.getUser_email().isEmpty())
                || (usr.getUser_first_name() != null && usr.getUser_first_name().isEmpty())
                || (usr.getUser_last_name() != null && usr.getUser_last_name().isEmpty())
                || (usr.getUser_pwd() != null && usr.getUser_pwd().length() < 8)){
            return  new ResponseEntity<>(userResponse, HttpStatus.valueOf(406));
        }

        if(usersRepository.hasEmail(usr.getUser_email())){
            userResponse.put("message", "User already exists");
            return new ResponseEntity<>(userResponse, HttpStatus.valueOf(409));
        }

        usr.setUser_pwd(bCryptPasswordEncoder.encode((usr.getUser_pwd())));
        usersRepository.insert(usr);
        userResponse.put("user", usr);
        return  new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    /**
     * Using CrudRepository save-method to update user
     * @param usr needs to update
     * @return updated user
     * @throws Exception
     */
    @PutMapping(path ="/update", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity updateUser(@Validated @RequestBody User usr,@RequestParam(name="sessionId") String sessionId,HttpServletRequest request) throws Exception{
        HashMap<String, Object> userResponse = new HashMap<>();
        //Get current session
        HttpSession currentSession = request.getSession();
        if(currentSession.getId().equals(sessionId) ){
            usr.setUser_pwd(bCryptPasswordEncoder.encode((usr.getUser_pwd())));
            usersRepository.save(usr);
            userResponse.put("user", usr);
            return  new ResponseEntity<>(userResponse, HttpStatus.OK);
        }
        //If session id expired delete session from db and make session invalidate
        else if(!currentSession.getId().equals(sessionId)){
            userSessionRepository.deleteUserSession(sessionId);
            currentSession.invalidate();
            return null;

        }

        return null;

    }

    /**
     * @param id user id who needs to deleted
     * @return response to client user was successfully deleted
     * @throws Exception
     */
    @DeleteMapping(path = "/delete")
    public ResponseEntity deleteUser(@Validated @RequestParam("id") String id,@RequestParam(name="sessionId") String sessionId,HttpServletRequest request)throws Exception{

        HashMap<String, Object> userResponse = new HashMap<>();
        //Get current session
        HttpSession currentSession = request.getSession();
        if(currentSession.getId().equals(sessionId) ){
            usersRepository.deleteById(id);
            userResponse.put("message", "User was successfully deleted");
            return  new ResponseEntity<>(userResponse, HttpStatus.OK);
        }
        //If session id expired delete session from db and make session invalidate
        else if(!currentSession.getId().equals(sessionId)){
            userSessionRepository.deleteUserSession(sessionId);
            currentSession.invalidate();
            return null;
        }

        return null;
    }

    /**
     *
     * @param login user email and password given by http request
     * @return if login succeeded, respond with new session id back to client
     * @throws Exception
     */
    @RequestMapping(path ="/login", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE},method = RequestMethod.POST)
    public ResponseEntity login(@RequestBody Login login, HttpServletRequest request) throws Exception{
        HashMap<String, Object> userResponse = new HashMap<>();


        if (usersRepository.getLoginUser(login.getUser_email(),login.getUser_pwd())){

            String sessionId = userSessionRepository.getUserSessionByUser_email(login.getUser_email());

            if(sessionId == null || sessionId.isEmpty()){
                sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
                userSessionRepository.addNewUserSession(sessionId,login.getUser_email());
            }

            // grab session id and return it back to the client as responds
            userResponse.put("SessionId",sessionId);
            return  new ResponseEntity<>(userResponse, HttpStatus.OK);
        }

        userResponse.put("message","No user found in MongoDB");
        return new ResponseEntity<>(userResponse,HttpStatus.OK);
    }

    @GetMapping("/logout")
    public ResponseEntity logout(@Validated @RequestParam(name="sessionId") String sessionId,HttpServletRequest request) throws Exception{
        HashMap<String, Object> userResponse = new HashMap<>();

        if (!sessionId.isEmpty()){
            userSessionRepository.deleteUserSession(sessionId);
            userResponse.put("Was deleted: ",sessionId);
            HttpSession sessionCurrent = request.getSession();
            sessionCurrent.invalidate();
            return  new ResponseEntity<>(userResponse, HttpStatus.OK);
        }
        userResponse.put("message","No sessionId found");
        return new ResponseEntity<>(userResponse,HttpStatus.OK);
    }

    @RequestMapping(path ="/getShopByLocation", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE},method = RequestMethod.POST)
    public List<User> searchUserByLocation(@Validated @RequestBody String location, HttpServletRequest request) throws Exception{

        Geo userLoc = geoRepository.getGeo(location);
        List<User> users = usersRepository.findAll();
        List<User> foundShops = new ArrayList<>();
        List<User> allShops = new ArrayList<>();
        for(User u: users){
            if(u.getSales_person() != null && u.getSales_person().getSales_person_location() != null){
                allShops.add(u);
                Geo shopLoc = geoRepository.getGeo(u.getSales_person().getSales_person_location().toString());
                if(Location.distance(userLoc, shopLoc, 'K') <= 20)
                    foundShops.add(u);
            }
        }

        ObjectMapper mapper = new ObjectMapper();
        JsonNode responseJsonNode = mapper.readTree(location);
        JsonNode loc = responseJsonNode.get("location");

        return !loc.asText().isEmpty() ? foundShops : allShops;
    }

    @GetMapping("/valid")
    public ResponseEntity validSession(@Validated @RequestParam(name="sessionId") String sessionId,HttpServletRequest request) throws Exception{
        HashMap<String, Object> userResponse = new HashMap<>();

        if (userSessionRepository.getUserSessionBySessionId(sessionId)){
            userResponse.put("message","valid session");
            return  new ResponseEntity<>(userResponse, HttpStatus.OK);
        }
        else{
            userResponse.put("message","SessionId not found");
            return new ResponseEntity<>(userResponse,HttpStatus.valueOf(404));
        }

    }

}




