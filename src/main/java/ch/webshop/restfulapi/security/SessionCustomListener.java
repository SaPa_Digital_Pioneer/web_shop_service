package ch.webshop.restfulapi.security;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *Using WebListener annotation to scan web listener bei Spring Boot instead of creating ->
 *a separated Spring Bean.
 *For using @WebListener it is important use @ServletComponentScan in Main SpringApplication
 */

@WebListener
public class SessionCustomListener implements HttpSessionListener {

        private final AtomicInteger activeSessions;

        public SessionCustomListener() {
            super();

            activeSessions = new AtomicInteger();
        }

    public SessionCustomListener(AtomicInteger activeSessions) {
        this.activeSessions = activeSessions;
    }

    public int getTotalActiveSession() {
            return activeSessions.get();
        }

        public void sessionCreated(HttpSessionEvent event) {
            activeSessions.incrementAndGet();
        }
        public void sessionDestroyed(HttpSessionEvent event) {
            activeSessions.decrementAndGet();
        }

}
